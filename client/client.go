package main

import (
	"net"
	"fmt"
	"../tool"
	"bufio"
	"os"
)

func main() {
	conn, err := net.Dial("tcp",tool.TypeServer)
	if err != nil {
		return
	}
	defer  conn.Close()

	inputData := make(chan string)
	fmt.Println("Can i help you?")
	go scanData(inputData)

	response := make(chan []string)
	quit := make(chan int)

	for{

		go tool.HandleConnectionData(conn,response,quit,true)

		for{
			select {
			case  x := <- inputData:
				//fmt.Println("client input:",x,"length:",len(x))
				tool.HandleSendData(conn,x)
			case responseData := <- response:
				for _,v := range responseData{
					fmt.Printf("%s ",v)
				}

			case <-quit:
				break
			}
		}
	}

}

func scanData(result chan string)  {
	input:= bufio.NewScanner(os.Stdin)
	for input.Scan(){
		if input.Text() == "end" { break }
		result <- input.Text()
	}

}


