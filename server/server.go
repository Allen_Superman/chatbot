package main

import (
	"fmt"
	"net"
	"../tool"
	"strings"
)

func main() {


	listen, err := net.Listen("tcp", tool.TypeServer)
	if err != nil {
		return
	}
	// close server if error
	defer listen.Close()

	fmt.Println("Begin listening....")

	// handle client connection info
	result := make(chan []string)
	quit := make(chan int)

	for {
		conn, err := listen.Accept()
		if err != nil {
			fmt.Println(err)
			break
		}

		go tool.HandleConnectionData(conn,result,quit,false)

		for  {
			select {
			case data := <-result:
				fmt.Println(data)
				natureLanguageHandle(conn,data)
				// handle data
			case <-quit:
				break
			}
		}

	}

}


func natureLanguageHandle(conn net.Conn,words []string) {
	answer:=""

	//not able to deal with too many words in a sentence
	if len(words)>9*2 {
	   answer=	"sentence is too complex"
		tool.HandleSendData(conn,answer)
		return
	}


	sentences:=make([]string,1,9)
	sentences[0]=""
	n:=len(words)
	if n>0 {
		// 去除空格
		// 去除换行符 if contains " " element in array , function still doesn't work
		for i:=0;i<len(words);i++{
 			words[i]=strings.Replace(words[i], " ", "", -1)
			words[i]=strings.Replace(words[i], "\n", "", -1)
		}

		for i:=0;i<len(words);i++{
			//here deal with "and" in a sentence
			if strings.EqualFold(words[i],"and") {
				if i==0 { //if "and" connect unuseful elements
				}else {
				m:=len(sentences)
				sentences=append(sentences,"")
				i++
				sentences[m]=sentences[m]+ words[i]
				}
				continue
			}

 			if i>0 {
				for m:=0;m<len(sentences);m++ {
					sentences[m]=sentences[m]+" "
				}
			}

			for m:=0;m<len(sentences);m++ {
				sentences[m]=sentences[m]+words[i]
			}

		}
	}

	for _,sentence :=range sentences{
 	  answer=answer+tool.FindAnswer(sentence)+"\n"
	}
	tool.HandleSendData(conn,answer)

}



