package tool

import (
"encoding/json"
"fmt"
"io/ioutil"
"os"
//"strings"
"log"
"regexp"
)

/** Answer MODEL  */
type Answer struct {
	Question string `json:"question"`
	Answer   string `json:"answer"`
}

/* It's like a database of the answer. But the format is Json */
type jsonContent struct {
	App     string   `json:"App"`
	Version float32  `json:"version"`
	Answers []Answer `json:"searchResults"`
}

func FindAnswer(question string) string {

	if len(question) == 0 {
		return "Can't handle "
	}

	var result string

	inputFile := "./res/searchResult.json"
	buf, err := ioutil.ReadFile(inputFile)
	if err != nil {
		fmt.Fprintf(os.Stderr, "File Error: %s\n", err)
	}

	var content jsonContent
	if err := json.Unmarshal(buf, &content); err != nil {
		log.Fatal(err)
	}

	result = "Please ask some question about the app."

	for _, a := range content.Answers {
		r, _ := regexp.Compile(a.Question)

		if r.MatchString(question) {
			result = a.Answer
			fmt.Println("match string:",r.MatchString(question),question,":::",a.Answer)
			break
		}
	}

	return result

}

//// IsNil 判断一个值是否为 nil。
//// 当特定类型的变量，已经声明，但还未赋值时，也将返回 true
//func IsNil(expr interface{}) bool {
//	if nil == expr {
//		return true
//	}
//
//	v := reflect.ValueOf(expr)
//	k := v.Kind()
//
//	return k >= reflect.Chan && k <= reflect.Slice && v.IsNil()
//}