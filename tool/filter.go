package tool

import (
	"strings"
)


func FilterData(buf []byte) []string {

	// Load json data
	filterFile := "./res/filter.json"
	jsonBuf := LoadJson(filterFile)
	filterJson := []string(jsonBuf)

	originData := SliceData(buf)

	//originSlice := originData[:]

	tmpData := make([]string, 0, len(originData))

	// add element
	for _, v1 := range originData {
		exist := false
		if v1 != " " {
			for _, v2 := range filterJson {
				if strings.Compare(v1, v2) == 0 {
					exist = true
				}
			}
			if !exist {
				tmpData = append(tmpData, v1)
			}

		}
	}

	return tmpData
}
