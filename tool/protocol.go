package tool

import (
	"net"
	"fmt"
)

const (
	TypeServer        = "127.0.0.1:11111"
	TypeBeginFlag     = 'b'
	TypeReadBufLength = 10
)

/*
	Send package rule :
		step 1 : send [a,b]  a: begin flag ,b : data length
		step 2 : send data to c/s
*/

func HandleSendData(conn net.Conn, originData string) {

	header := make([]byte, 0, 2)
	length := len(originData)

	if length > 0 {

		// send header
		header = append(header, TypeBeginFlag, byte(length))
		conn.Write(header)
		//fmt.Println("Data header:", header)

		// send data body
		dataBody := StringToBytes(originData)
		//fmt.Println("Data body:", dataBody)
		conn.Write(dataBody)
	}

}

/*
	get data rule:
		1)get data size 10 as default
		2). step 1. if data length > 10 ,
						get data size 10 first ,
			step 2.		if the last size > 10 ,
							get another 10 sie data , continue step 2
						else
							get the last size data
			step 3. if get all data ,over
*/

func HandleConnectionData(conn net.Conn, result chan []string, quit chan int,response bool) {

	headerBuf := make([]byte, 2, 2)
	var length int = TypeReadBufLength
	var total int = 0
	var totalBuf []byte

	for {

		n, err := conn.Read(headerBuf)
		if err != nil {
			fmt.Println(err)
			break
			quit <- 0
		}

		//fmt.Println(n,headerBuf)
		if n == 0 {
			quit <- 0
		}

		if n == 2 && headerBuf[0] == TypeBeginFlag { // header data

			// begin flag ,header
			dataLength := int(headerBuf[1])
			//fmt.Println(dataLength)

			headerBuf = ClearSlice(headerBuf)
			length = dataLength
			if dataLength > TypeReadBufLength { // read  tool.TypeReadBufLength size data
				headerBuf = make([]byte, TypeReadBufLength, TypeReadBufLength)
			} else { // read dataLength size data
				headerBuf = make([]byte, dataLength, dataLength)
			}

		} else { // body data

			if n == length { //read all data

				totalBuf = append(totalBuf, headerBuf...)
				resultData := FilterData(totalBuf)

				// clear data
				headerBuf = ClearSlice(headerBuf)
				headerBuf = make([]byte, 2, 2)
				totalBuf = make([]byte,0,0)
				length = TypeReadBufLength

				result <- resultData

			} else { // read part data

				if n == TypeReadBufLength {

					total += n
					totalBuf = append(totalBuf, headerBuf...)
					if length-total <= TypeReadBufLength { // less tool.TypeReadBufLength
						headerBuf = make([]byte, length-total, length-total)
						headerBuf = ClearSlice(headerBuf)
					}

				} else {

					total += n
					totalBuf = append(totalBuf, headerBuf...)
				}

				if total == length {
					if !response {
						resultData := FilterData(totalBuf)
						result <- resultData
					}else {
						result <- []string{string(totalBuf)}
					}

					// clear data
					headerBuf = ClearSlice(headerBuf)
					headerBuf = make([]byte, 2, 2)
					totalBuf = make([]byte,0,0)
					length = TypeReadBufLength
					total = 0

				}
			}

		}
	}
}
