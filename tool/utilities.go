package tool

import (
	"io/ioutil"
	"fmt"
	"encoding/json"
	"github.com/go-ego/gse"
	"reflect"
	"unsafe"
)



var hasLoadDict bool
var segmenter gse.Segmenter

func LoadJson(filename string) []string {

	data, err := ioutil.ReadFile(filename)
	if err != nil {
		fmt.Println("Load json error:", err)
		return nil
	}
	fmt.Println("json data:", data)

	dataJson := []byte(data)
	var result []string
	err = json.Unmarshal(dataJson, &result)
	if err != nil {
		return nil
	}

	return result
}

func SliceData(buf []byte) []string {

	if !hasLoadDict {
		segmenter.LoadDict()
		hasLoadDict = true
	}
	segments := segmenter.Segment(buf)
	dict := gse.ToSlice(segments, true)

	return dict
}


func StringToBytes(s string) (b []byte) {
	pbytes := (*reflect.SliceHeader)(unsafe.Pointer(&b))
	pstring := (*reflect.StringHeader)(unsafe.Pointer(&s))
	pbytes.Data = pstring.Data
	pbytes.Len = pstring.Len
	pbytes.Cap = pstring.Len
	return
}

func ClearSlice(slice []byte)[]byte {

	for i := range slice{
		slice[i] = byte(0)
	}
	return slice
}
